**Yalantis Test**

This is API application that allow manage vehicles and drivers
Below you can see examples and enpoints for API requests

GET

Get all drivers

http://127.0.0.1:5000/drivers/driver/

Get list of all drivers

BODYurlencoded

Example Request

Get all drivers


```python
import requests

url = "http://127.0.0.1:5000/drivers/driver/"

payload={}
headers = {}

response = requests.request("GET", url, headers=headers, data=payload)

print(response.text)
```

GET

Get one driver

http://127.0.0.1:5000/drivers/driver/2

Get one driver by ID

Example Request

Get one driver


```python
import requests

url = "http://127.0.0.1:5000/drivers/driver/2"

payload={}
headers = {}

response = requests.request("GET", url, headers=headers, data=payload)

print(response.text)
```

POST

Post New Driver

http://127.0.0.1:5000/drivers/driver/

Post new driver to DB

HEADERS

Content-Type

application/x-www-form-urlencoded

BODYurlencoded

first_name

Alexey

last_name

Novikov

Example Request

Post New Driver



```python
import requests

url = "http://127.0.0.1:5000/drivers/driver/"

payload='first_name=Alexey&last_name=Novikov'
headers = {
  'Content-Type': 'application/x-www-form-urlencoded'
}

response = requests.request("POST", url, headers=headers, data=payload)
```

PATCH

Patch a driver

http://127.0.0.1:5000/drivers/driver/2?new_first_name=Harry&new_last_name=Rowling 6

Patch a driver in DB

HEADERS

Content-Type

application/x-www-form-urlencoded

PARAMS

new_first_name

Harry

new_last_name

Rowling 6

BODYurlencoded

Example Request

Patch a driver


```python
import requests

url = "http://127.0.0.1:5000/drivers/driver/2?new_first_name=Harry&new_last_name=Rowling 6"

payload={}
headers = {
  'Content-Type': 'application/x-www-form-urlencoded'
}

response = requests.request("PATCH", url, headers=headers, data=payload)
```

GET

Get drivers created_at__gte=10-11-2021

http://127.0.0.1:5000/drivers/driver/?created_at__gte=10-11-2021

Get list of all drivers created_at__gte=10-11-2021

PARAMS

created_at__gte

10-11-2021

Example Request

Get drivers created_at__gte=10-11-2021

View More


```python
import requests

url = "http://127.0.0.1:5000/drivers/driver/?created_at__gte=10-11-2021"

payload={}
headers = {}

response = requests.request("GET", url, headers=headers, data=payload)

print(response.text)
```

GET

Get drivers created_at__lte=10-11-2021

http://127.0.0.1:5000/drivers/driver/?created_at__lte=10-11-2021

Get list of all drivers created_at__lte=10-11-2021

PARAMS

created_at__lte

10-11-2021

Example Request

Get drivers created_at__lte=10-11-2021

View More


```python
import requests

url = "http://127.0.0.1:5000/drivers/driver/?created_at__lte=10-11-2021"

payload={}
headers = {}

response = requests.request("GET", url, headers=headers, data=payload)

print(response.text)
```

DEL

Delete a driver

http://127.0.0.1:5000/drivers/driver/1

Delete a driver from DB

HEADERS

Content-Type

application/x-www-form-urlencoded

BODYurlencoded

Example Request

Delete a driver




```python
import requests

url = "http://127.0.0.1:5000/drivers/driver/1"

payload={}
headers = {
  'Content-Type': 'application/x-www-form-urlencoded'
}

response = requests.request("DELETE", url, headers=headers, data=payload)
DEL
```


Delete a vehicle

http://127.0.0.1:5000//vehicles/vehicle/2

Delete a vehicle from DB

HEADERS

Content-Type

application/x-www-form-urlencoded

BODYurlencoded

Example Request

Delete a vehicle




```python
import requests

url = "http://127.0.0.1:5000//vehicles/vehicle/2"

payload={}
headers = {
  'Content-Type': 'application/x-www-form-urlencoded'
}

response = requests.request("DELETE", url, headers=headers, data=payload)
```

POST

Post New Vehicle

http://127.0.0.1:5000/vehicles/vehicle/

Post new vehicle to DB

HEADERS

Content-Type

application/x-www-form-urlencoded

BODYurlencoded

make

MAN

model

TRUCK

plate_number

AA 2334 BB

Example Request

Post New Vehicle




```python
import requests

url = "http://127.0.0.1:5000/vehicles/vehicle/"

payload='make=MAN&model=TRUCK&plate_number=AA%202334%20BB'
headers = {
  'Content-Type': 'application/x-www-form-urlencoded'
}

response = requests.request("POST", url, headers=headers, data=payload)
```

GET

Get all vehicles

http://127.0.0.1:5000/vehicles/vehicle/

Get list of all vehicles

Example Request

Get all vehicles




```python
import requests

url = "http://127.0.0.1:5000/vehicles/vehicle/"

payload={}
headers = {}

response = requests.request("GET", url, headers=headers, data=payload)

print(response.text)
```

GET

Get all vehicles without drivers

http://127.0.0.1:5000/vehicles/vehicle/?with_drivers=no

Get list of all vehicles without drivers

PARAMS

with_drivers

no

Example Request

Get all vehicles without drivers




```python
import requests

url = "http://127.0.0.1:5000/vehicles/vehicle/?with_drivers=no"

payload={}
headers = {}

response = requests.request("GET", url, headers=headers, data=payload)

print(response.text)
```

GET

Get all vehicles with drivers

http://127.0.0.1:5000/vehicles/vehicle/?with_drivers=yes

Get list of all vehicles with drivers

PARAMS

with_drivers

yes

Example Request

Get all vehicles with drivers




```python
import requests

url = "http://127.0.0.1:5000/vehicles/vehicle/?with_drivers=yes"

payload={}
headers = {}

response = requests.request("GET", url, headers=headers, data=payload)

print(response.text)
```

GET

Get one Vehicle

http://127.0.0.1:5000/vehicles/vehicle/1

Get one vehicle by ID

Example Request

Get one Vehicle




```python
import requests

url = "http://127.0.0.1:5000/vehicles/vehicle/1"

payload={}
headers = {}

response = requests.request("GET", url, headers=headers, data=payload)

print(response.text)
```

PATCH

Patch a vehicle

http://127.0.0.1:5000/vehicles/vehicle/1?new_make=Mercedes&new_model=Benz&new_plate_number=BB 5555 BB

Patch a vehicle in DB

HEADERS

Content-Type

application/x-www-form-urlencoded

PARAMS

new_make

Mercedes

new_model

Benz

new_plate_number

BB 5555 BB

BODYurlencoded

Example Request

Patch a vehicle



```python
import requests

url = "http://127.0.0.1:5000/vehicles/vehicle/1?new_make=Mercedes&new_model=Benz&new_plate_number=BB 5555 BB"

payload={}
headers = {
  'Content-Type': 'application/x-www-form-urlencoded'
}

response = requests.request("PATCH", url, headers=headers, data=payload)
```

POST

Get in certain driver in vehicle

http://127.0.0.1:5000/vehicles/set_driver/4

Get driver in from vehicle

HEADERS

Content-Type

application/x-www-form-urlencoded

PARAMS

driver_id

4

BODYurlencoded

driver_id

4

Example Request

Get in certain driver in vehicle



```python
import requests

url = "http://127.0.0.1:5000/vehicles/set_driver/4"

payload='driver_id=4'
headers = {
  'Content-Type': 'application/x-www-form-urlencoded'
}

response = requests.request("POST", url, headers=headers, data=payload)
```

POST

Get out driver from vehicle

http://127.0.0.1:5000/vehicles/set_driver/4

Get driver out from vehicle

HEADERS

Content-Type

application/x-www-form-urlencoded

BODYurlencoded

driver_id

4

Example Request

Get out driver from vehicle



```python
import requests

url = "http://127.0.0.1:5000/vehicles/set_driver/4"

payload={}
headers = {
  'Content-Type': 'application/x-www-form-urlencoded'
}

response = requests.request("POST", url, headers=headers, data=payload)
```
