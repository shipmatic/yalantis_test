**Installation of server to local machine**

1. Make clone of project from Gitlab:
     ```git clone 
	 
	 	url = "https://gitlab.com/shipmatic/yalantis_test.git"
	 ```
2. Create and run virtual enviroment: virtualenv venv,  .\activate venv
3. Install all dependencies from requirements.txt: pip install -r requrements.txt
4. Replace local DB with own 
     ```
	 app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///yalantis_test.db'
	 
	 ```
5. Run local server
	
