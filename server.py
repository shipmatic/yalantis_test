import datetime
from flask import Flask, render_template, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from flask_bootstrap import Bootstrap
from sqlalchemy.orm import relationship


app = Flask(__name__, static_url_path='/static')
# app.config['SECRET_KEY'] = 'MySuperSecretKey'
Bootstrap(app)

# Connect to Database
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///yalantis_test.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

# Drivers and Vehicles TABLE Configuration


class Driver(db.Model):
    __tablename__ = 'drivers'
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(50), nullable=False)
    last_name = db.Column(db.String(50), nullable=False)
    created_at = db.Column(db.Date, nullable=False,
                           default=datetime.datetime.now())
    updated_at = db.Column(db.Date, nullable=False,
                           default=datetime.datetime.now())
    vehicle = relationship("Vehicle")

    def to_dict(self):
        return {column.name: getattr(self, column.name) for column in self.__table__.columns}


class Vehicle(db.Model):
    __tablename__ = 'vehicles'
    id = db.Column(db.Integer, primary_key=True)
    make = db.Column(db.String(50), nullable=False)
    model = db.Column(db.String(50), nullable=False)
    plate_number = db.Column(db.String(50), nullable=False)
    created_at = db.Column(db.Date, nullable=False,
                           default=datetime.datetime.now())
    updated_at = db.Column(db.Date, nullable=False,
                           default=datetime.datetime.now())
    driver_id = db.Column(db.Integer, db.ForeignKey("drivers.id"))

    def to_dict(self):
        return {column.name: getattr(self, column.name) for column in self.__table__.columns}


db.create_all()


@app.route("/")
def home():
    return render_template("index.html")

# API ENDPOINTS HERE
################### DRIVERS####################


@app.route("/drivers/driver/", methods=["POST"])
def post_new_driver():

    new_driver = Driver(

        first_name=request.form.get("first_name"),
        last_name=request.form.get("last_name"),      
    )
    db.session.add(new_driver)
    db.session.commit()
    return jsonify(response={"success": "Successfully added the new driver."})

# HTTP PUT/PATCH - Update Record


@app.route("/drivers/driver/<int:driver_id>", methods=["PATCH"])
def patch_driver(driver_id):
    new_first_name = request.args.get("new_first_name")
    new_last_name = request.args.get("new_last_name")

    driver = db.session.query(Driver).get(driver_id)
    if driver:
        driver.first_name = new_first_name
        driver.last_name = new_last_name
        driver.updated_at = datetime.datetime.now()
        db.session.commit()
        # Just add the code after the jsonify method. 200 = Ok
        return jsonify(response={"success": "Successfully updated the Driver."}), 200
    else:
        # 404 = Resource not found
        return jsonify(error={"Not Found": "Sorry a driver with that id was not found in the database."}), 404

# HTTP DELETE - Delete Record


@app.route("/drivers/driver/<driver_id>", methods=["GET", "POST", "DELETE"])
def delete_driver(driver_id):
    driver = db.session.query(Driver).get(driver_id)
    if driver:
        db.session.delete(driver)
        db.session.commit()
        return jsonify(success={"SUCCESS": "Entry Deleted."})
    else:
        return jsonify(error={"Not Found": "Sorry a driver with that id was not found in the database."}), 404


@app.route("/drivers/driver/<int:driver_id>", methods=["GET"])
def one_driver(driver_id):
    driver = db.session.query(Driver).get(driver_id)
    return jsonify([driver.to_dict()])


@app.route("/drivers/driver/", methods=["GET"])
def all_drivers():
    created_at__gte = request.args.get("created_at__gte")
    created_at__lte = request.args.get("created_at__lte")

    if created_at__gte:
        dt = datetime.datetime.strptime(created_at__gte, '%d-%m-%Y')
        query_date = dt.strftime('%Y-%m-%d')
        drivers = Driver.query.filter(Driver.created_at >= query_date).all()
    elif created_at__lte:
        dt = datetime.datetime.strptime(created_at__lte, '%d-%m-%Y')
        query_date = dt.strftime('%Y-%m-%d')
        drivers = Driver.query.filter(Driver.created_at <= query_date).all()

    else:
        drivers = db.session.query(Driver).all()

    return jsonify(drivers=[driver.to_dict() for driver in drivers])


################### VEHICLES ####################
# HTTP DELETE - Delete Record
@app.route("/vehicles/vehicle/<vehicle_id>", methods=["GET", "POST", "DELETE"])
def delete_vehicle(vehicle_id):
    vehicle = db.session.query(Vehicle).get(vehicle_id)
    if vehicle:
        db.session.delete(vehicle)
        db.session.commit()
        return jsonify(success={"SUCCESS": "Entry Deleted."})
    else:
        return jsonify(error={"Not Found": "Sorry a vehicle with that id was not found in the database."}), 404

# POST New Vehicle


@app.route("/vehicles/vehicle/", methods=["POST"])
def post_new_vehicle():

    new_vehicle = Vehicle(
        make=request.form.get("make"),
        model=request.form.get("model"),
        plate_number=request.form.get("plate_number")      
    )
    db.session.add(new_vehicle)
    db.session.commit()
    return jsonify(response={"success": "Successfully added the new vehicle."})

# GET all Vehicles


@app.route("/vehicles/vehicle/", methods=["GET"])
def all_vehicles():
    with_drivers = request.args.get("with_drivers")
    print(with_drivers)
    if with_drivers == "yes":
        vehicles = Vehicle.query.filter(Vehicle.driver_id != None).all()
    elif with_drivers == "no":
        vehicles = Vehicle.query.filter(Vehicle.driver_id == None).all()

    else:
        vehicles = db.session.query(Vehicle).all()

    return jsonify(vehicles=[vehicle.to_dict() for vehicle in vehicles])

# GET info about certain vehicle


@app.route("/vehicles/vehicle/<int:vehicle_id>", methods=["GET"])
def one_vehicle(vehicle_id):
    vehicle = db.session.query(Vehicle).get(vehicle_id)
    return jsonify([vehicle.to_dict()])


# HTTP PUT/PATCH - Update Vehicle
@app.route("/vehicles/vehicle/<int:vehicle_id>", methods=["PATCH"])
def patch_vehicle(vehicle_id):
    new_make = request.args.get("new_make")
    new_model = request.args.get("new_model")
    new_plate_number = request.args.get("new_plate_number") 

    vehicle = db.session.query(Vehicle).get(vehicle_id)
    if vehicle:
        vehicle.make = new_make
        vehicle.model = new_model
        vehicle.plate_number = new_plate_number
        vehicle.updated_at = datetime.datetime.now()
        db.session.commit()
        # Just add the code after the jsonify method. 200 = Ok
        return jsonify(response={"success": "Successfully updated the Vehicle."}), 200
    else:
        # 404 = Resource not found
        return jsonify(error={"Not Found": "Sorry a vehicle with that id was not found in the database."}), 404


# POST /vehicles/set_driver/<vehicle_id>/  driver get in and out from vehicle
# @app.route("/vehicles/set_driver/<int:vehicle_id>", methods=["PATCH"])
# def get_driver_in_out(vehicle_id):
#     vehicle = db.session.query(Vehicle).get(vehicle_id)
#     driver = request.args.get("driver_id")

#     if driver:
#         vehicle.driver_id = int(driver)
#         vehicle.updated_at = datetime.datetime.now()

#     else:
#         vehicle.driver_id = None
#         vehicle.updated_at = datetime.datetime.now()
#     db.session.commit()

#     return jsonify(response={"success": "Successfully updated the Vehicle."}), 200

@app.route("/vehicles/set_driver/<int:vehicle_id>", methods=["POST"])
def get_driver_in_out(vehicle_id):
    vehicle = db.session.query(Vehicle).get(vehicle_id)
    driver = request.form.get("driver_id")

    if driver:
        vehicle.driver_id = int(driver)
        vehicle.updated_at = datetime.datetime.now()

    else:
        vehicle.driver_id = None
        vehicle.updated_at = datetime.datetime.now()
    db.session.commit()

    return jsonify(response={"success": "Successfully updated the Vehicle."}), 200

# DRIVERS ROUTES 
# @app.route("/drivers")
# def drivers():
#     all_drivers = db.session.query(Driver).all()
#     return render_template('drivers.html', drivers = all_drivers)


# @app.route("/drivers/add", methods = ["GET", "POST"])
# def add_driver():
#     form = DriverForm()

#     if form.validate_on_submit():
#         print('form passed')

#         first_name = form.first_name.data
#         last_name = form.last_name.data

#         new_driver = Driver(first_name=first_name, last_name=last_name)
#         db.session.add(new_driver)
#         db.session.commit()

#         return redirect(url_for('drivers'))

    # return render_template('add_driver.html', form=form)

# @app.route('/drivers/<int:id>', methods=['GET', 'POST'])
# def edit_driver(id):
#     form = DriverEditForm()
#     id = id
#     print(id)
#     if form.validate_on_submit():
#         driver_to_update = Driver.query.filter_by(id=id).first()
#         driver_to_update.first_name = form.first_name.data
#         driver_to_update.last_name = form.last_name.data
#         driver_to_update.updated_at = datetime.datetime.now()
#         db.session.commit()  
#         return redirect(url_for('drivers'))

#     return render_template('add_driver.html', form=form)

# @app.route('/drivers/delete')
# def delete_driver():

#     driver_to_delete = Driver.query.get(request.args.get('id'))
#     db.session.delete(driver_to_delete)
#     db.session.commit()

#     return redirect(url_for('home'))

# VEHICLES ROUTES

# @app.route("/vehicles")
# def vehicles():
#     all_vehicles = db.session.query(Vehicle).all()
#     return render_template('vehicles.html', vehicles = all_vehicles)

# @app.route("/vehicles/add", methods = ["GET", "POST"])
# def add_vehicle():
#     form = VehicleForm()

#     if form.validate_on_submit():
#         print('form passed')

#         make = form.make.data
#         model = form.model.data
#         plate_number = form.plate_number.data
#         driver_id = form.driver_id.data
#         new_vehicle = Vehicle(make=make, model=model,plate_number = plate_number, driver_id = driver_id)
#         db.session.add(new_vehicle)
#         db.session.commit()

#         return redirect(url_for('vehicles'))

#     return render_template('add_vehicle.html', form=form)

# @app.route('/vehicles/<int:id>', methods=['GET', 'POST'])
# def edit_vehicle(id):
#     form = VehicleEditForm()
#     id = id
#     print(id)
#     if form.validate_on_submit():
#         vehicle_to_update = Vehicle.query.filter_by(id=id).first()

#         vehicle_to_update.make = form.make.data
#         vehicle_to_update.model = form.model.data
#         vehicle_to_update.plate_number = form.plate_number.data
#         vehicle_to_update.driver_id = form.driver_id.data
#         vehicle_to_update.updated_at = datetime.datetime.now()
#         db.session.commit()  
#         return redirect(url_for('vehicles'))

#     return render_template('add_vehicle.html', form=form)

# @app.route('/vehicles/delete')
# def delete_vehicle():

#     vehicle_to_delete = Vehicle.query.get(request.args.get('id'))
#     db.session.delete(vehicle_to_delete)
#     db.session.commit()

#     return redirect(url_for('vehicles'))

if __name__ == '__main__':
    app.run(debug=True)
