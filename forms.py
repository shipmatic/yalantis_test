from flask_bootstrap import Bootstrap
from flask_wtf import FlaskForm, form
from wtforms import StringField, SubmitField
# from wtforms.fields.core import SelectField
from wtforms.fields.datetime import DateTimeField
from wtforms.validators import DataRequired


#CREATE RECORD
# new_driver = Driver(id=2, first_name="Harry", last_name="Potter", created_at = datetime.datetime.now(), updated_at = datetime.datetime.now())
# db.session.add(new_driver)
# new_car = Vehicle(id=3, driver_id = 2, make = "Renault", model = "Magnum", plate_number = "AC 4331 AA", created_at = datetime.datetime.now(), updated_at = datetime.datetime.now() )
# db.session.add(new_car)
# db.session.commit()

class DriverForm(FlaskForm):
    first_name = StringField('First Name', validators=[DataRequired()])
    last_name = StringField('Last Name', validators=[DataRequired()])
    # created_at = DateTimeField('Created', validators=[DataRequired()])
    submit = SubmitField('Submit')

class DriverEditForm(FlaskForm):
    first_name = StringField('First Name', validators=[DataRequired()])
    last_name = StringField('Last Name', validators=[DataRequired()])
    # created_at = DateTimeField('Created', validators=[DataRequired()])
    submit = SubmitField('Submit')


class VehicleForm(FlaskForm):
    make = StringField('Maker', validators=[DataRequired()])
    model = StringField('Model', validators=[DataRequired()])
    plate_number = StringField('Plate Number', validators=[DataRequired()])
    driver_id = StringField('Driver ID', validators=[DataRequired()])
    submit = SubmitField('Submit')

class VehicleEditForm(FlaskForm):
    make = StringField('Maker', validators=[DataRequired()])
    model = StringField('Model', validators=[DataRequired()])
    plate_number = StringField('Plate Number', validators=[DataRequired()])
    driver_id = StringField('Driver ID', validators=[DataRequired()])
    submit = SubmitField('Submit')